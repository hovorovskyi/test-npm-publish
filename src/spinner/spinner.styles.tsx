import styled from 'styled-components';

export const SpinnerOverlay = styled.div`
  height: 60vh;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const SpinnerCircle = styled.div`
  display: inline-block;
  width: 3.5rem;
  height: 3.5rem;
  border: 3px solid red;
  border-radius: 50%;
  border-top-color: white;
  border-right-color: white;
  animation: spin 1s ease-in-out infinite;

  @keyframes spin {
    to {
      transform: rotate(360deg);
    }
  }
`;
