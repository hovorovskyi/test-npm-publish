import { Meta, StoryObj } from '@storybook/react';
import Spinner from './spinner.component';

const meta = {
  title: 'Spinner',
  component: Spinner,
  parameters: {},
  tags: ['autodocs'],
} satisfies Meta<typeof Spinner>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {};
