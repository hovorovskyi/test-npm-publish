import { SpinnerCircle, SpinnerOverlay } from './spinner.styles';

const Spinner = () => (
  <SpinnerOverlay data-testid='spinner'>
    <SpinnerCircle />
  </SpinnerOverlay>
);

export default Spinner;
